unit ui;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,system.DateUtils, ShellApi, superobject,superxmlparser,
  System.JSON, System.JSONConsts,REST.Types, REST.Client, REST.Authenticator.Basic, Data.Bind.Components, Data.Bind.ObjectScope,
  URLMon,  System.StrUtils,
  mv_yhteiset, Vcl.ExtCtrls;

  const HOST='https://www.veikkaus.fi';
  const ASETUKSET_TXT='asetukset_vakio.txt';
  const HAETUT_PERCS_TXT='haetut_percs.txt';
  const MAX_KOHTEITA=40;
  const AIKAERO_TUNTEJA=3;
type
  TForm1 = class(TForm)
    ButtonAloitaHaku: TButton;
    HTTPBasicAuthenticator1: THTTPBasicAuthenticator;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    RESTClient1: TRESTClient;
    MemoPelilista: TMemo;
    ButtonHaePelilista: TButton;
    ComboBoxKohteet: TComboBox;
    MemoStatus: TMemo;
    Timer1: TTimer;
    EditInterval: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    CheckBoxTimestamp: TCheckBox;
    CheckBoxJatka: TCheckBox;
    LabelStatus: TLabel;
    ButtonHaetut_percs_txt: TButton;
    procedure ButtonHaePelilistaClick(Sender: TObject);
    procedure ButtonAloitaHakuClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonHaetut_percs_txtClick(Sender: TObject);
  private
    { Private declarations }
    T_ID:array[0..MAX_KOHTEITA] of integer;
    //esim. pelattavana kohteet 1, 2, 6
    //-> 0=1, 1=2, 3=6
    T_ComboBoxMapKohde: array [0..MAX_KOHTEITA] of word;
    T_Otteluita:array[0..20] of integer;
    T_Joukkueet:array[0..20] of array[0..2] of string;
    T_vakioOtteluita:array[1..20] of integer;

    T_Percs:array[1..20] of array[1..18] of array[1..3] of double;

    FiPaivitetty:integer;
    FiVaihto:integer;
    FsHaettavaSivu:string;
    FiKohde:integer;

    function SuoritusajanEsitystapa(Suoritusaika: integer): string;
    procedure LisaaStatusRivi(Status: string);

    procedure HaePelattu(const sHaettavaSivu:string;const iKohde:integer);
    function HaePelilista:integer;
    //Procedure KohdetiedotMemoon(sNro:string);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


function TForm1.SuoritusajanEsitystapa(Suoritusaika: integer): string;
var
iMin,iSec,iMilliSec:integer;
begin
  iMin:= Suoritusaika div 60000;
  iSec:= (Suoritusaika mod 60000) div 1000;
  iMillisec:= (Suoritusaika mod 60000) mod 1000;

  Result := format('%d min %d.%d s', [Suoritusaika div 60000, (Suoritusaika mod 60000) div 1000, (Suoritusaika mod 60000) mod 1000 ]);
end;

procedure TForm1.LisaaStatusRivi(Status: string);
begin
  MemoStatus.Lines.Add(format('%d: %s', [MemoStatus.Lines.Count, Status]));

  form1.MemoStatus.SelStart := 0;
  form1.MemoStatus.Perform(EM_ScrollCaret, 0, 0);
  SendMessage(MemoStatus.Handle, EM_LINESCROLL, 0, MemoStatus.Lines.Count);
end;


procedure TForm1.Timer1Timer(Sender: TObject);
begin
  HaePelattu(FsHaettavaSivu, FiKohde);
end;

// 4x faster than dateutils version
function UNIXTimeToDateTimeFAST(UnixTime: LongWord): TDateTime;
begin
Result := (UnixTime / 86400) + 25569;
end;

// 10x faster than dateutils version
function DateTimeToUNIXTimeFAST(DelphiTime : TDateTime): LongWord;
begin
Result := Round((DelphiTime - 25569) * 86400);
end;


//Timer tekee t�m�n
procedure TForm1.HaePelattu(const sHaettavaSivu:string; const iKohde:integer);
var
 json         : ISuperObject;
 popularity_item     : ISuperObject;
 row_item     : ISuperObject;
 outcome_item: ISuperObject;
 item: TSuperObjectIter;
 jValue:TJSONValue;
 jDraw:TJSONValue;
 sDraw,sRivi: string;
 i,j:integer;
 iCurrentKohde,iOtteluita,iDrawID:integer;
 sDrawID:string;
 iOttelu,iMerkki:integer;
 dPros:double;
 sMerkki:string;
 iPaivitetty:integer;
 txt:textFile;
 sDateTime:string;
 iVaihto:integer;
 dVaihto:double;
 sStatus:string;
 dtPaivitetty:TDateTime;
 iErotus:integer;
 begin
//  (sender as ttimer).enabled := false;


  //      LisaaStatusRivi('HaePelattu');
 //-------------------------
  form1.RestClient1.BaseURL:=sHaettavaSivu;
  form1.RESTRequest1.Execute;
 // GET /api/v1/sport-games/draws/SPORT/{drawId}/popularity

  jValue:=form1.RESTResponse1.JSONValue;
  iCurrentKohde:=0;
  if (jValue is TJSONObject) then begin
      json:=TSuperObject.ParseString(PWideChar(JValue.ToString),TRUE);
      iPaivitetty:=json['timestamp'].AsInteger div 1000;

      //muunnetaan Veikkauksen timestamp UnixTime:ksi
      //t�ytyy jakaa ylemm�ss�, muuten menee v��rin, miksi??
      //iPaivitetty:=iPaivitetty div 1000;
      iPaivitetty:=iPaivitetty+(60*60*AIKAERO_TUNTEJA);

      dtPaivitetty:= UNIXTimeToDateTimeFAST( iPaivitetty );
      iVaihto:=json['exchange'].AsInteger;

      iErotus:=iPaivitetty-DateTimeToUNIXTimeFAST(now);

      if iVaihto>FiVaihto then begin
        FiPaivitetty:=iPaivitetty;
        FiVaihto:=iVaihto;


        AssignFile(txt,HAETUT_PERCS_TXT );
        if (fileExists(HAETUT_PERCS_TXT)) and (checkboxJatka.Checked) then  Append(Txt) else rewrite(txt);


        sDateTime:=inttostr(iPaivitetty);
  //      sRivi:=sDateTime+';';
//        sRivi:=sRivi+FloatToStrF(FVaihto,FFfixed,12,2);
        sRivi:=IntToStr(iVaihto);

        for popularity_item in json['resultPopularities'] do begin// iterate through draws array
          iOttelu:=popularity_item['eventId'].asInteger+1;
          dPros:=popularity_item['percentage'].asDouble;

          sMerkki:=uppercase(popularity_item['outcomes'].asArray[0].AsString);
          if sMerkki ='1' then iMerkki:=1 else
          if sMerkki ='X' then iMerkki:=2 else
          if sMerkki ='2' then iMerkki:=3;

          T_Percs[iKohde,iOttelu,iMerkki]:=dPros;

          if iMerkki=1 then sRivi:=sRivi+';'+FloatToStrF(dPros,FFfixed,6,0) else
          sRivi:=sRivi+','+FloatToStrF(dPros,FFfixed,6,0);
        end;

        dVaihto:=iVaihto/100;
        sStatus:=floattostrF(dVaihto,FFCurrency,7,2);
        sStatus:=datetimetostr(dtPaivitetty)+' : '+sStatus;//+'    Updated: '+datetimetostr(dtPaivitetty);//+ ' erotus: '+inttostr(iErotus)+' Original Timestamp: '+inttostr(iPaivitetty) ;

        LisaaStatusRivi(sStatus);

        if checkboxTimestamp.checked then sRivi:=sRivi+'|'+sDateTime;
        writeln(txt,sRivi);


        //json:=TSuperObject.ParseString(PWideChar(JValue.ToString),TRUE);
       closefile(txt);


      end;
 
  end;

 // LisaaStatusRivi(jVAlue.toString);

  form1.MemoPelilista.SelStart := 0;
  form1.MemoPelilista.Perform(EM_ScrollCaret, 0, 0);

  if iCurrentKohde>0 then begin
    form1.comboboxKohteet.itemindex:=iCurrentKohde-1;
    iOtteluita:=t_vakioOtteluita[iCurrentKohde];
    //form1.radioGroupKohteita.itemindex:=iOtteluita-8;
  end;

  //-------------------
 //   sender.free;
end;


function TForm1.HaePelilista:integer;
var
 json         : ISuperObject;
 draws_item     : ISuperObject;
 row_item     : ISuperObject;
 outcome_item: ISuperObject;
 item: TSuperObjectIter;
 jValue:TJSONValue;
 sRivi: string;
 i,j,iKohde, draw_index:integer;
 iEkaKohde,iLisatty:integer;
begin
//  vanha url:
//	'https://www.veikkaus.fi/api/odj/v2/sport-games/draws?game-names=MULTISCORE'
//  uusi url:
//	'https://www.veikkaus.fi/api/sport-open-games/v1/games/MULTISCORE/draws'
//  vanha url:
//  form1.RestClient1.BaseURL:=HOST+'/api/v1/sport-games/draws?game-names=SPORT';
//  uusi url:
  MemoPelilista.Clear;
  for I := 0 to MAX_KOHTEITA do T_ID[i]:=0;


  RestClient1.BaseURL:=HOST+'/api/sport-open-games/v1/games/SPORT/draws';
  RESTRequest1.Execute;
  result:=0;
  iEkaKohde:=0;//

  jValue:=RESTResponse1.JSONValue;

  if (jValue is TJSONArray) then begin

  json:=TSuperObject.ParseString(PWideChar(JValue.ToString),TRUE);

  for draw_index := 0 to json.AsArray.Length - 1 do
  begin
    draws_item := json.AsArray[draw_index];
 // for draws_item in json['draws'] do begin// iterate through draws array
   if (draws_item['status'].AsString='OPEN') then begin
    i:=0;
    iKohde:=draws_item['listIndex'].AsInteger;
    sRivi:=('Vakio '+intToStr(iKohde)+'. id: '+draws_item['id'].AsString);//+row_item['closeTime'].AsString);
    if iEkaKohde=0 then iEkaKohde:=iKohde;
    T_ID[iKohde]:=draws_item['id'].AsInteger;
    //{"draws":[{"gameName":"SPORT","brandName":"4","id":"51507","name":"Jalkapallovakio","status":"OPEN","openTime":1416110400000,"closeTime":1416326100000,"drawTime":1416261600000,"resultsAvailableTime":1416261600000,"gameRuleSet":{"basePrice":10,"maxPrice":300000,"additionalPrizeTierAvailable":true,"additionalPrizeTierPrice":10,"quickPickAvailable":true,"oddsType":"VARIABLE"},"rows":[{"id":"0","tvChannel":"Veikkau
    //"rows":[{"id":"0","tvChannel":"Veikkaus","outcome":{"home":{"name":"Slovakia"},"away":{"name":"Suomi"}},"eventId":"86974971","additionalPrizeTier":true},
    MemoPelilista.Lines.Add(sRivi);

    if result=0 then result:=iKohde;

 //   MemoContent.Lines.Add(JValue.ToString) ;

    for row_item in draws_item['rows'] do begin
       //kohteen numero=id+1
        i:=i+1;
//       sRivi:=inttostr(row_item['id'].AsInteger+1);//+'. '+row_item['name'].AsString;
         j:=0;
       for outcome_item in row_item['outcome'] do begin

         sRivi:='';
         begin if ObjectFindFirst(outcome_item, item) then repeat
				   if item.key='name'  then begin
            j:=j+1;
 //          showmessage(skoti+' '+item.val.AsString);
            T_Joukkueet[i,j]:=item.val.AsString;
           end;
         until not ObjectFindNext(item);
         ObjectFindClose(item);
       end;

        //MemoContent.Lines.Add(row_item.AsString);
    end;

     sRivi:=inttostr(row_item['id'].AsInteger+1)+ '. '+T_Joukkueet[i,1]+'-'+T_Joukkueet[i,2];
        MemoPelilista.Lines.Add(sRivi);
   end;//row_item in draws_item['rows']

    //otteluiden m��r�: 13,12,11...
    t_Otteluita[iKohde]:=i;
    MemoPelilista.Lines.Add('');
   end;//if (draws_item['status'].AsString='OPEN') then begin

   end;//for draws_item in json['draws'] do begin// iterate through draws array
    //MemoContent.Text:= jValue.ToString
  end else MemoPelilista.Text:= RESTResponse1.Content;
//  showmessage(RESTResponse1.ContentType);
  form1.MemoPelilista.SelStart := 0;
  form1.MemoPelilista.Perform(EM_ScrollCaret, 0, 0);

  if iEkaKohde>0 then begin
    comboboxKohteet.clear;
    iLisatty:=0;
    for I := 0 to MAX_KOHTEITA do begin
      if T_ID[i]>0 then  begin
        iLisatty:=iLisatty+1;
        ComboboxKohteet.Items.Add('Vakio '+inttostr(i));
        T_ComboBoxMapKohde[iLisatty-1]:=i;
      end;
    end;

    comboboxKohteet.itemindex:=0;

 //   MonivetoMemoon(inttostr(iCurrentKohde));
  end;


end;

 {
Procedure TForm1.KohdetiedotMemoon(sNro:string);
var
  txt:TextFile;
  rivi:string;
  i:integer;
  FileName:string;
  FileDate:integer;
  Kohteet: TArray<String>;
begin
      Filename:='vakio_'+ sNro+'.txt';

      if FileExists(Pchar(Filename))  then begin
          AssignFile(txt, Pchar(Filename));
          Reset(txt) ;
          MemoKohteentiedot.clear;

          readln(txt,rivi); //Perustiedot
          MemoKohteentiedot.Lines.add(rivi);

          readln(txt,rivi); //Ottelutiedot + aika
          MemoKohteentiedot.Lines.add(rivi);
          Kohteet := rivi.Split([';']);

          fileDate := FileAge(Filename);

          // Did we get the file age OK?
          if fileDate > -1 then begin
            rivi:=(fileName+' last modified = '+  DateToStr(FileDateToDateTime(fileDate))+' '+
                TimeToStr(FileDateToDateTime(fileDate)));
            MemoKohteentiedot.Lines.add(rivi);
          end;

          closefile(txt);
      end;
end;
 }


procedure TForm1.ButtonAloitaHakuClick(Sender: TObject);
var
  iIntervalMilliSekunteja:integer;
  iKohde,iDrawID:integer;
  sDrawId:string;
begin
   if comboboxKohteet.ItemIndex=-1 then showmessage('Valitse ensin pelattava peli.') else begin
      iIntervalMilliSekunteja:=strtoint(editInterval.Text);

      if Timer1.Enabled=false then begin
          FiVaihto:=0;
          iKohde:=T_ComboBoxMapKohde[comboboxKohteet.ItemIndex];
          FiKohde:=iKohde;
          iDrawId:=T_ID[iKohde];
          if iDrawId>0 then begin
            sDrawID:=inttostr(iDrawId);
            FsHaettavaSivu:=HOST+'/api/sport-popularity/v1/games/SPORT/draws/'+sdrawID+'/popularity';
            LabelStatus.caption:=FsHaettavaSivu+'...';

            ButtonAloitaHaku.Caption:='Keskeyt� haku';
            timer1.Interval := iIntervalMilliSekunteja;
            timer1.Enabled := True;
          end else showmessage('DrawID = '+sDrawID);
      end else begin
        timer1.Enabled:=false;
        lisaaStatusRivi('Haku keskeytetty ( Vakio '+inttostr(FiKohde)+' ).');
        ButtonAloitaHaku.Caption:='Aloita haku';
        LabelStatus.caption:='';
      end;
      checkboxJatka.Checked:=true;
   end;
end;


procedure TForm1.ButtonHaePelilistaClick(Sender: TObject);
var
 iKohde:integer;
begin
  iKohde := HaePelilista;
  if iKohde>0 then begin
    comboboxKohteet.itemindex:=iKohde-1;
//    KohdetiedotMemoon(inttostr(iKohde));
//    EditNro.Text:=inttostr(iKohde);
  end;
end;

procedure TForm1.ButtonHaetut_percs_txtClick(Sender: TObject);
begin
    ShellExecute(Handle, 'open', 'notepad.exe',HAETUT_PERCS_TXT,nil, SW_SHOWNORMAL) ;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  iKohde:integer;
begin
  self.Caption:=Application.ExeName;
  LabelStatus.Caption:='';
  Timer1.Enabled:=False;
  memoStatus.Clear;
  iKohde := HaePelilista;
  if iKohde>0 then begin
    comboboxKohteet.itemindex:=iKohde-1;
//    KohdetiedotMemoon(inttostr(iKohde));
//    EditNro.Text:=inttostr(iKohde);
  end;

 // Timer1.OnTimer:=TForm;
end;

end.
